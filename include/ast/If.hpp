#ifndef FREEMIX_AST_IF_HPP
#define FREEMIX_AST_IF_HPP

#include "Statement.hpp"
#include "Value.hpp"

namespace freemix::ast {

class If final:
    public Statement
{
public:
    using IfPtr = std::shared_ptr<If>;
    using StatementPtr = Statement::StatementPtr;
    using ValuePtr = Value::ValuePtr;

    struct IfThen
    {
        ValuePtr condition;
        List<StatementPtr> statement;
    };

    If() noexcept;
    ~If() noexcept override;
    If(const If & other);
    If(If && other) noexcept;
    auto operator =(const If & other) -> If & = delete;
    auto operator =(If && other) noexcept -> If & = delete;
    auto swap(If & other) noexcept -> void;

    [[nodiscard]] auto thens() const noexcept -> const List<IfThen> &;
    [[nodiscard]] auto elseStm() const noexcept -> const List<StatementPtr> &;
    [[nodiscard]] auto thens() noexcept -> List<IfThen> &;
    [[nodiscard]] auto elseStm() noexcept -> List<StatementPtr> &;

private:
    List<IfThen> _thens;
    List<StatementPtr> _else;
};

} // namespace freemix::ast

#endif
