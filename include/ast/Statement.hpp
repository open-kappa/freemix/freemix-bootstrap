#ifndef FREEMIX_AST_STATEMENT_HPP
#define FREEMIX_AST_STATEMENT_HPP

#include "Ast.hpp"

namespace freemix::ast {

class Statement:
    public Ast
{
public:
    using StatementPtr = std::shared_ptr<Statement>;

    ~Statement() noexcept override = 0;

protected:
    Statement() noexcept;
    Statement(const Statement & other);
    Statement(Statement && other) noexcept;

    auto swap(Statement & other) noexcept -> void;

private:
    auto operator =(const Statement & other) -> Statement & = delete;
    auto operator =(Statement && other) noexcept -> Statement & = delete;
};

} // namespace freemix::ast

#endif
