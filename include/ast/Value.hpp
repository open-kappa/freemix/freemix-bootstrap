#ifndef FREEMIX_AST_VALUE_HPP
#define FREEMIX_AST_VALUE_HPP

#include "Ast.hpp"

namespace freemix::ast {

class Value:
    public Ast
{
public:
    using ValuePtr = std::shared_ptr<Value>;

    ~Value() noexcept override = 0;

protected:
    Value() noexcept;
    Value(const Value & other);
    Value(Value && other) noexcept;

    auto swap(Value & other) noexcept -> void;

private:
    auto operator =(const Value & other) -> Value & = delete;
    auto operator =(Value && other) noexcept -> Value & = delete;
};

} // namespace freemix::ast

#endif
