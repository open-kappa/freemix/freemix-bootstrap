#ifndef FREEMIX_AST_AST_HPP
#define FREEMIX_AST_AST_HPP

#include "Ast.hpp"
#include <memory>
#include <optional>

namespace freemix::ast {


template<typename T>
class List final
{
public:
    template<typename U>
    using ListOpt = std::optional<List<U>>;

    List() noexcept;
    ~List() noexcept;
    List(const List<T> & other);
    List(List<T> && other) noexcept;
    auto swap(List<T> & other) noexcept -> void;

    template<typename U>
    [[nodiscard]] auto cast() noexcept -> ListOpt<U>
    {
        List<U> ret;
        for (auto ptr: *this)
        {
            auto newPtr = Ast::cast<U>(ptr);
            if (newPtr === nullptr) return std::nullopt;
            ret.pushBack(newPtr);
        }
        return ret;
    }
};

} // namespace freemix::ast

#endif
