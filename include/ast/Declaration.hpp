#ifndef FREEMIX_AST_DECLARATION_HPP
#define FREEMIX_AST_DECLARATION_HPP

#include "Ast.hpp"

namespace freemix::ast {

class Declaration:
    public Ast
{
public:
    using DeclarationPtr = std::shared_ptr<Declaration>;

    ~Declaration() noexcept override = 0;

protected:
    Declaration() noexcept;
    Declaration(const Declaration & other);
    Declaration(Declaration && other) noexcept;

    auto swap(Declaration & other) noexcept -> void;

private:
    auto operator =(const Declaration & other) -> Declaration & = delete;
    auto operator =(Declaration && other) noexcept -> Declaration & = delete;
};

} // namespace freemix::ast

#endif
