#ifndef FREEMIX_AST_AST_HPP
#define FREEMIX_AST_AST_HPP

#include <memory>

namespace freemix::ast {

class Ast
{
public:
    using AstPtr = std::shared_ptr<Ast>;

    virtual ~Ast() noexcept = 0;

    [[nodiscard]] auto parent() noexcept -> AstPtr;
    [[nodiscard]] auto parent() const noexcept -> const AstPtr;

    [[nodiscard]] virtual auto clone(const bool deep) -> Ast;
    [[nodiscard]] auto clonePtr(const bool deep) -> AstPtr;
    [[nodiscard]] virtual auto preVisit(Visitor & visitor) -> bool = 0;
    [[nodiscard]] virtual auto postVisit(Visitor & visitor) -> bool = 0;
    [[nodiscard]] virtual auto preVisit(Visitor & visitor) const -> bool = 0;
    [[nodiscard]] virtual auto postVisit(Visitor & visitor) const -> bool = 0;

    template<typename T, typename V>
    constexpr inline static auto cast(std::shared_ptr<V> & value) -> T &
    {
        return std::dynamic_pointer_cast<T>(value);
    }

protected:
    Ast() noexcept;
    Ast(const Ast & other);
    Ast(Ast && other) noexcept;

    auto swap(Ast & other) noexcept -> void;

    AstPtr _parent;

private:
    auto operator =(const Ast & other) -> Ast & = delete;
    auto operator =(Ast && other) noexcept -> Ast & = delete;
};

} // namespace freemix::ast

#endif
