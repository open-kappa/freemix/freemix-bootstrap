#ifndef FREEMIX_LEXER_MATCHER_HPP
#define FREEMIX_LEXER_MATCHER_HPP

#include "../common/common.hpp"
#include <functional>
#include <string>
#include <vector>


namespace freemix::lexer {
class Lexer;
struct MatchResult;
} // namespace freemix::lexer

namespace freemix::lexer {

using Matcher = std::function<auto(Lexer & lexer) -> MatchResult>;

} // namespace freemix::lexer

#endif
