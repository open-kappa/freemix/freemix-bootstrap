#ifndef FREEMIX_LEXER_CONTEXT_HPP
#define FREEMIX_LEXER_CONTEXT_HPP

#include "../common/common.hpp"
#include "Matcher.hpp"
#include <string>
#include <vector>

namespace freemix::lexer {

class Context final
{
public:
    struct ContextIdTag {};

    using SourceInfo = freemix::common::SourceInfo;
    using ContextId = freemix::common::Id<ContextIdTag>;
    using Matchers = std::vector<Matcher>;

    Context() noexcept;
    ~Context() noexcept;
    Context(const Context & other);
    Context(Context && other) noexcept;
    auto operator =(const Context & other) -> Context &;
    auto operator =(Context && other) noexcept -> Context &;
    auto swap(Context & other) noexcept -> void;
    explicit Context(const ContextId id) noexcept;

    [[nodiscard]] auto id() const -> ContextId;
    [[nodiscard]] auto matchers() const noexcept -> const Matchers &;
    [[nodiscard]] auto matchers() noexcept -> Matchers &;

    [[nodiscard]] static auto noContext() noexcept -> ContextId;
    [[nodiscard]] static auto keepContext() noexcept -> ContextId;
    [[nodiscard]] static auto defaultContext() noexcept -> ContextId;

private:
    ContextId _id;
    Matchers _matchers;
};

auto swap(Context & o1, Context & o2) noexcept -> void;

} // namespace freemix::lexer

#endif
