#ifndef FREEMIX_LEXER_MATCHRESULT_HPP
#define FREEMIX_LEXER_MATCHRESULT_HPP

#include "Context.hpp"
#include "Token.hpp"
#include <string>
#include <vector>


namespace freemix::lexer {

struct MatchResult final
{
    std::string remaining;
    std::vector<Token> tokens = {};
    Context::ContextId nextContext = Context::keepContext();
    bool matched = false;
};

} // namespace freemix::lexer

#endif
