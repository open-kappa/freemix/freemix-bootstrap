#ifndef FREEMIX_LEXER_MATCHERDEFS_HPP
#define FREEMIX_LEXER_MATCHERDEFS_HPP

#include "../common/common.hpp"
#include "Context.hpp"
#include "Matcher.hpp"
#include "Token.hpp"
#include <functional>
#include <string>
#include <vector>


namespace freemix::lexer {
class Lexer;
struct MatchResult;
} // namespace freemix::lexer

namespace freemix::lexer {

[[nodiscard]] auto makeRegExpMatcher(
    const std::string & regexp,
    const std::vector<Token::TokenId> & ids,
    const Context::ContextId & nextContext
) -> Matcher;

[[nodiscard]] auto makeStringMatcher(
    const std::string & str,
    const Token::TokenId & id,
    const Context::ContextId & nextContext
) -> Matcher;

} // namespace freemix::lexer

#endif
