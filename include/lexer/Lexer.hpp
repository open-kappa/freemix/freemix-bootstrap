#ifndef FREEMIX_LEXER_LEXER_HPP
#define FREEMIX_LEXER_LEXER_HPP

#include "Context.hpp"
#include "../common/common.hpp"
#include "Token.hpp"
#include <list>
#include <map>
#include <memory>
#include <string>

namespace freemix::lexer {

class Lexer final
{
public:
    using SourceInfo = freemix::common::SourceInfo;
    using ContextId = Context::ContextId;
    using ContextMap = std::map<ContextId, Context>;
    using IStreamPtr = std::shared_ptr<std::istream>;
    struct InputInfo
    {
        std::string file;
        IStreamPtr stream;
    };
    using IStreamStack = std::vector<InputInfo>;
    using InTokens = std::list<Token>;
    struct StackInfo final
    {
        std::list<Token> tokens{};
    };
    using Stack = std::list<StackInfo>;
    using ScopedGuard = freemix::common::ScopedGuard;

    Lexer() noexcept;
    ~Lexer() noexcept;
    Lexer(const Lexer & other);
    Lexer(Lexer && other) noexcept;
    auto operator =(const Lexer & other) -> Lexer &;
    auto operator =(Lexer && other) noexcept -> Lexer &;
    auto swap(Lexer & other) noexcept -> void;

    auto pushIStream(
        const std::string & file,
        const IStreamPtr & istream
    ) -> void;
    [[nodiscard]] auto hasToken() -> bool;
    [[nodiscard]] auto token() -> const Token &;
    auto untoken() -> void;
    auto pushStackTokens() -> void;
    auto popStackTokens() -> void;
    [[nodiscard]] auto scopedContext() -> ScopedGuard;

    [[nodiscard]] auto currentSource() const noexcept -> const SourceInfo &;
    [[nodiscard]] auto currentContext() const -> const Context &;
    [[nodiscard]] auto context(const ContextId & id) const -> const Context &;
    [[nodiscard]] auto context(const ContextId & id) -> Context &;
    [[nodiscard]] auto context() const -> const Context &;
    [[nodiscard]] auto context() -> Context &;

private:
    auto _fetchLine() -> void;
    auto _tokenize() -> void;

    mutable ContextMap _contextMap;
    IStreamStack _inputs;
    InTokens _inTokens;
    Stack _stack;

    std::string _currentLine;
    ContextId _currentContext;
    SourceInfo _sourceInfo;
};

auto swap(Lexer & o1, Lexer & o2) noexcept -> void;

} // namespace freemix::lexer

#endif
