#ifndef FREEMIX_LEXER_TOKEN_HPP
#define FREEMIX_LEXER_TOKEN_HPP

#include "../common/common.hpp"
#include <compare>
#include <string>

namespace freemix::lexer {

constexpr auto NO_TOKEN_ID = freemix::common::Size(0);

class Token final
{
public:
    struct TokenIdTag {};
    using TokenId = freemix::common::Id<TokenIdTag>;
    using SourceInfo = freemix::common::SourceInfo;
    using Value = std::string;

    Token() noexcept;
    ~Token() noexcept;
    Token(const Token & other);
    Token(Token && other) noexcept;
    auto operator =(const Token & other) -> Token &;
    auto operator =(Token && other) noexcept -> Token &;
    auto swap(Token & other) noexcept -> void;
    Token(
        const SourceInfo & sourceInfo,
        const TokenId & id,
        const Value & value
    );

    [[nodiscard]] auto sourceInfo() const noexcept -> const SourceInfo &;
    auto sourceInfo(const SourceInfo & sourceInfo) noexcept -> Token &;

    [[nodiscard]] auto id() const noexcept -> const TokenId &;
    auto id(const TokenId & id) noexcept -> Token &;

    [[nodiscard]] auto value() const noexcept -> const Value &;
    auto value(const Value & value) noexcept -> Token &;

    [[nodiscard]] auto operator ==(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator !=(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator <(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator <=(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator >(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator >=(const Token & other) const noexcept -> bool;
    [[nodiscard]] auto operator <=>(
        const Token & other
    ) const noexcept -> std::strong_ordering;

    [[nodiscard]] static auto noTokenId() noexcept -> TokenId;

private:
    SourceInfo _sourceInfo;
    TokenId _id;
    Value _value;
};

auto swap(Token & o1, Token & o2) noexcept -> void;

} // namespace freemix::lexer

#endif
