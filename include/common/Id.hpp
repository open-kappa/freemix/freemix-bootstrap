#ifndef FREEMIX_COMMON_ID_HPP
#define FREEMIX_COMMON_ID_HPP

#include <compare>
#include <cstdlib>
#include <utility>

namespace freemix::common {

template<typename T>
class Id final
{
public:
    enum : std::size_t
    {
        NO_ID = 0
    };

    constexpr Id() noexcept:
        _id(NO_ID)
    {}

    ~Id() noexcept
    {}

    constexpr Id(const Id & other) noexcept:
        _id(other._id)
    {}

    constexpr Id(Id && other) noexcept:
        _id(other._id)
    {
        other._id = NO_ID;
    }

    constexpr auto operator =(const Id & other) noexcept -> Id &
    {
        Id tmp(other);
        swap(tmp);
        return *this;
    }

    constexpr auto operator =(Id && other) noexcept -> Id &
    {
        Id tmp{std::move(other)};
        swap(tmp);
        return *this;
    }

    constexpr auto swap(Id & other) noexcept -> void
    {
        using std::swap;
        swap(_id, other._id);
    }

    [[nodiscard]] constexpr auto operator ==(const Id & other) const noexcept -> bool
    {
        return _id == other._id;
    }

    [[nodiscard]] constexpr auto operator !=(const Id & other) const noexcept -> bool
    {
        return _id != other._id;
    }

    [[nodiscard]] constexpr auto operator <(const Id & other) const noexcept -> bool
    {
        return _id < other._id;
    }

    [[nodiscard]] constexpr auto operator <=(const Id & other) const noexcept -> bool
    {
        return _id <= other._id;
    }

    [[nodiscard]] constexpr auto operator >(const Id & other) const noexcept -> bool
    {
        return _id > other._id;
    }

    [[nodiscard]] constexpr auto operator >=(const Id & other) const noexcept -> bool
    {
        return _id >= other._id;
    }

    [[nodiscard]] constexpr auto operator <=>(const Id & other) const noexcept -> std::strong_ordering
    {
        return _id <=> other._id;
    }

    [[nodiscard]] static auto newId() noexcept -> Id
    {
        static auto newId = NO_ID;
        newId += 1;
        return Id{newId};
    }

    [[nodiscard]] constexpr static auto noId() noexcept -> Id
    {
        return Id();
    }

private:
    constexpr explicit Id(const size_t id) noexcept:
        _id(id)
    {}

    std::size_t _id;
};

template<typename T>
constexpr auto swap(Id<T> & o1, Id<T> & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::common

#endif
