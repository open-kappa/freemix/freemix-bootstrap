#ifndef FREEMIX_COMMON_SCOPEDGUARD_HPP
#define FREEMIX_COMMON_SCOPEDGUARD_HPP

#include <functional>

namespace freemix::common {

class ScopedGuard final
{
public:
    using Callback = std::function<auto() -> void>;

    ScopedGuard() noexcept;
    ~ScopedGuard();
    ScopedGuard(ScopedGuard && other) noexcept;
    auto operator =(ScopedGuard && other) noexcept -> ScopedGuard &;
    auto swap(ScopedGuard & other) noexcept -> void;
    explicit ScopedGuard(const Callback & cb) noexcept;

    auto release() noexcept -> void;

private:
    Callback _callback;

    ScopedGuard(const ScopedGuard & other) = delete;
    auto operator =(const ScopedGuard & other) -> ScopedGuard & = delete;
};

auto swap(ScopedGuard & o1, ScopedGuard & o2) noexcept -> void;

} // namespace freemix::common

#endif
