#ifndef FREEMIX_COMMON_TYPES_HPP
#define FREEMIX_COMMON_TYPES_HPP

#include <cstdlib>

namespace freemix::common {

using Size = std::size_t;

} // namespace freemix::common

#endif
