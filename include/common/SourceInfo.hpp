#ifndef FREEMIX_COMMON_SOURCEINFO_HPP
#define FREEMIX_COMMON_SOURCEINFO_HPP

#include "./types.hpp"
#include <string>

namespace freemix::common {

class SourceInfo final
{
public:
    SourceInfo() noexcept;
    ~SourceInfo() noexcept;
    SourceInfo(const SourceInfo & other);
    SourceInfo(SourceInfo && other) noexcept;
    auto operator =(const SourceInfo & other) -> SourceInfo &;
    auto operator =(SourceInfo && other) noexcept -> SourceInfo &;
    auto swap(SourceInfo & other) noexcept -> void;
    SourceInfo(
        const std::string & file,
        const Size line,
        const Size column
    );

    [[nodiscard]] auto operator ==(const SourceInfo & other) const noexcept -> bool;
    [[nodiscard]] auto operator !=(const SourceInfo & other) const noexcept -> bool;

    [[nodiscard]] auto file() const noexcept -> const std::string &;
    [[nodiscard]] auto line() const noexcept -> Size;
    [[nodiscard]] auto column() const noexcept -> Size;

    auto file(const std::string & file) -> SourceInfo &;
    auto line(const Size line) noexcept -> SourceInfo &;
    auto column(const Size column) noexcept -> SourceInfo &;

private:
    std::string _file;
    Size _line;
    Size _column;
};

auto swap(SourceInfo & o1, SourceInfo & o2) noexcept -> void;

} // namespace freemix::common

#endif
