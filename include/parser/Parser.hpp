#ifndef FREEMIX_PARSER_PARSER_HPP
#define FREEMIX_PARSER_PARSER_HPP

#include "../lexer/lexer.hpp"

namespace freemix::parser {

class Parser final
{
public:
    using Lexer = freemix::lexer::Lexer;

    Parser() noexcept;
    virtual ~Parser() noexcept;
    Parser(const Parser & other);
    Parser(Parser && other) noexcept;
    auto operator =(const Parser & other) -> Parser & = delete;
    auto operator =(Parser && other) noexcept -> Parser & = delete;
    auto swap(Parser & other) noexcept -> void;

    auto lexer() noexcept -> Lexer &;
    auto lexer() const noexcept -> const Lexer &;

private:
    Lexer _lexer;
};

auto swap(Parser & o1, Parser & o2) noexcept -> void;

} // namespace freemix::parser

#endif
