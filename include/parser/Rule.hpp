#ifndef FREEMIX_PARSER_RULE_HPP
#define FREEMIX_PARSER_RULE_HPP

#include "RuleImpl.hpp"
#include "../common/common.hpp"
#include "../lexer/lexer.hpp"
#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace freemix::parser {
class Ast;
class Parser;
} // namespace freemix::parser

namespace freemix::parser {

template<typename TAst>
class Rule final
{
public:
    using RuleImplPtr = RuleTraits<Rule<TAst>, TAst>::RuleImplPtr;
    using Matcher = RuleTraits<Rule<TAst>, TAst>::Matcher;
    using MatcherPtr = RuleTraits<Rule<TAst>, TAst>::MatcherPtr;
    using Matchers = RuleTraits<Rule<TAst>, TAst>::Matchers;
    using Action = RuleTraits<Rule<TAst>, TAst>::Action;

    Rule() noexcept:
        _impl()
    {}

    ~Rule() noexcept
    {}

    Rule(const Rule<TAst> & other):
        _impl(other._impl)
    {}

    Rule(Rule<TAst> && other) noexcept:
        _impl(std::move(other._impl))
    {}

    auto operator =(const Rule<TAst> & other) -> Rule<TAst> &
    {
        Rule<TAst> tmp(other);
        swap(tmp);
        return *this;
    }

    auto operator =(Rule<TAst> && other) noexcept -> Rule<TAst> &
    {
        Rule<TAst> tmp(std::move(other));
        swap(tmp);
        return *this;
    }

    auto swap(Rule<TAst> & other) -> void
    {
        using std::swap;
        swap(_impl, other._impl);
    }

    [[nodiscard]] auto operator()(Parser & parser) const -> ParseResult<TAst>
    {
        return reinterpret_cast<ParseResult<TAst> &&>(_impl(parser));
    }

    [[nodiscard]] auto name() const -> const std::string &
    {
        return _impl.name();
    }

    [[nodiscard]] auto matchers() const -> const Matchers &
    {
        return _impl.matchers();
    }

    [[nodiscard]] auto matchers() -> Matchers
    {
        return _impl.matchers();
    }

private:
    RuleImpl _impl;
};

template<typename TAst>
auto swap(Rule<TAst> & o1, Rule<TAst> & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::parser

#endif
