#ifndef FREEMIX_PARSER_TOKENRULE_HPP
#define FREEMIX_PARSER_TOKENRULE_HPP

#include "Parser.hpp"
#include "../common/common.hpp"
#include "../lexer/lexer.hpp"
#include "Rule.hpp"
#include <functional>
#include <string>
#include <vector>

namespace freemix::parser {

template<typename T>
using TokenMapper =
    std::function<auto(const freemix::lexer::Token & token) -> T>;

template<typename T>
auto matchTokenType(
    const std::string & name,
    const freemix::lexer::Token::TokenId & id,
    const TokenMapper<T> & mapper
) -> Rule<T>
{
    return Rule<T>()
        .name(name)
        .matchers().push_back(std::make_shared<>);
}

template<typename T>
auto matchTokenValue(
    const std::string & value,
    const TokenMapper<T> & mapper
) -> Rule<T>
{
    return [value, mapper](Parser & parser) -> ParseResult<T>
    {
        ParseResult<T> ret;
        if (!parser.lexer().hasToken()) return ret;
        const auto & token = parser.lexer().token();
        if (token.value() != value) return ret;
        ret.ast.push_back(mapper(token));
        ret.matched = true;
        return ret;
    };
}

template<typename T>
auto matchTokenTypeAndValue(
    const freemix::lexer::Token::TokenId & id,
    const std::string & value,
    const TokenMapper<T> & mapper
) -> Rule<T>
{
    return [id, value, mapper](Parser & parser) -> ParseResult<T>
    {
        ParseResult<T> ret;
        if (!parser.lexer().hasToken()) return ret;
        const auto & token = parser.lexer().token();
        if (token.id() != id) return ret;
        if (token.value() != value) return ret;
        ret.ast.push_back(mapper(token));
        ret.matched = true;
        return ret;
    };
}

template<typename ... T, typename TRet>
auto makeRule(Rule<T> ... rules) -> Rule<TRet>
{
    std::array<Rule<TRet>, sizeof...(T)> allRules = {
        rules...,
    };
    return [allRules](Parser & parser) -> ParseResult<TRet>
    {
        ParseResult<TRet> ret;
        auto guard = parser.lexer().scopedContext();
        for (auto & rule: allRules)
        {
            const auto & res = rule(parser);
            if (!res.matched)
            {
                ret.ast.clear();
                return ret;
            }
            for (auto & node: res.ast)
            {
                ret.ast.push_back(node);
            }
        }
        ret.matched = true;
        guard.release();
        return ret;
    };
}

template<typename T>
auto matchList(
    const Rule<T> matcher,
    const Rule<T> separator
) -> Rule<T>
{

}


} // namespace freemix::parser

#endif
