#ifndef FREEMIX_PARSER_RULEIMPL_HPP
#define FREEMIX_PARSER_RULEIMPL_HPP

#include "../common/common.hpp"
#include "../lexer/lexer.hpp"
#include <functional>
#include <memory>
#include <string>
#include <vector>


namespace freemix::parser {
class Ast;
class Parser;
} // namespace freemix::parser

namespace freemix::parser {

template<typename T>
using AstList = std::vector<std::shared_ptr<T>>;

using Code = uint64_t;

struct Info
{
    freemix::common::SourceInfo source;
    Code code;
};

using InfoList = std::vector<Info>;

template<typename T>
struct ParseResult
{
    bool matched = false;
    AstList<T> ast;
    InfoList _warnings;
    InfoList _errors;
};

template<typename TRuleClass, typename TAst>
struct RuleTraits
{
    using RuleImplPtr = std::shared_ptr<TRuleClass>;
    using Matcher = std::function<auto(Parser &) -> ParseResult<Ast>>;
    using MatcherPtr = std::shared_ptr<Matcher>;
    using Matchers = std::vector<MatcherPtr>;
    using Action = std::function<auto(AstList<Ast> & ins) -> ParseResult<TAst>>;
};

class RuleImpl final
{
public:
    using RuleImplPtr = RuleTraits<RuleImpl, Ast>::RuleImplPtr;
    using Matcher = RuleTraits<RuleImpl, Ast>::Matcher;
    using MatcherPtr = RuleTraits<RuleImpl, Ast>::MatcherPtr;
    using Matchers = RuleTraits<RuleImpl, Ast>::Matchers;
    using Action = RuleTraits<RuleImpl, Ast>::Action;

    RuleImpl() noexcept;
    ~RuleImpl() noexcept;
    RuleImpl(const RuleImpl & other);
    RuleImpl(RuleImpl && other) noexcept;
    auto operator =(const RuleImpl & other) -> RuleImpl &;
    auto operator =(RuleImpl && other) noexcept -> RuleImpl &;
    auto swap(RuleImpl & other) noexcept -> void;

    [[nodiscard]] auto operator()(Parser & parser) const -> ParseResult<Ast>;

    [[nodiscard]] auto name() const -> const std::string &;
    auto name(const std::string & name) -> RuleImpl &;
    [[nodiscard]] auto matchers() const -> const Matchers &;
    [[nodiscard]] auto matchers() -> Matchers;
    [[nodiscard]] auto action() const noexcept -> const Action &;
    auto action(const Action & action) -> RuleImpl &;

private:
    std::string _name;
    Matchers _matchers;
    Action _action;
};

auto swap(RuleImpl & o1, RuleImpl & o2) noexcept -> void;

} // namespace freemix::parser

#endif
