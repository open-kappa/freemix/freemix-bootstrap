#include "../include/lexer/lexer.hpp"
#include <cstdlib>
#include <iostream>
#include <sstream>


using Context = freemix::lexer::Context;
using Lexer = freemix::lexer::Lexer;
using SourceInfo = freemix::common::SourceInfo;
using Token = freemix::lexer::Token;

using freemix::lexer::makeRegExpMatcher;
using freemix::lexer::makeStringMatcher;


namespace /*anon*/ {

auto test_matchRegexp() -> bool
{
    std::cout << "test_matchRegexp() ..." << std::endl;

    std::stringstream ss;
    const std::string file = "tmpFile";
    const std::string num = "123456789";
    const std::string chars = "ABC";
    ss << num << chars;
    Lexer::IStreamPtr istream{&ss};

    Lexer lexer;
    lexer.pushIStream(file, istream);

    const auto & tokenId = Token::TokenId::newId();

    auto & context = lexer.context();
    context.matchers().push_back(makeRegExpMatcher(
        "([0-9]*)",
        {tokenId},
        Context::defaultContext()
    ));

    if (!lexer.hasToken()) return false;
    const auto & token = lexer.token();
    if (token.id() != tokenId) return false;
    if (token.sourceInfo() != SourceInfo(file, 1, 1)) return false;
    if (token.value() != num) return false;
    if (lexer.currentSource() != SourceInfo(file, 1, num.size() + 1)) return false;

    std::cout << "test_matchRegexp(): success" << std::endl;
    return true;
}

auto test_noMatchRegexp() -> bool
{
    std::cout << "test_noMatchRegexp() ..." << std::endl;

    std::stringstream ss;
    const std::string file = "tmpFile";
    const std::string num = "123456789";
    const std::string chars = "ABC";
    ss << chars << num;
    Lexer::IStreamPtr istream{&ss};

    Lexer lexer;
    lexer.pushIStream(file, istream);

    if (lexer.hasToken()) return false;

    std::cout << "test_noMatchRegexp(): success" << std::endl;
    return true;
}

auto test_skipMatchRegexp() -> bool
{
    std::cout << "test_skipMatchRegexp() ..." << std::endl;

    std::stringstream ss;
    const std::string file = "tmpFile";
    const std::string num = "123456789";
    const std::string chars = "ABC";
    ss << chars << num << chars;
    Lexer::IStreamPtr istream{&ss};

    Lexer lexer;
    lexer.pushIStream(file, istream);

    const auto & tokenId = Token::TokenId::newId();

    auto & context = lexer.context();
    context.matchers().push_back(makeRegExpMatcher(
        ".*([0-9]*)",
        {tokenId},
        Context::defaultContext()
    ));

    if (!lexer.hasToken()) return false;
    const auto & token = lexer.token();
    if (token.id() != tokenId) return false;
    if (token.sourceInfo() != SourceInfo(file, 1, chars.size() + 1)) return false;
    if (token.value() != num) return false;
    if (lexer.currentSource() != SourceInfo(file, 1, chars.size()+ num.size() + 1)) return false;

    std::cout << "test_skipMatchRegexp(): success" << std::endl;
    return true;
}

auto test_moreMatches() -> bool
{
    std::cout << "test_moreMatches() ..." << std::endl;

    std::stringstream ss;
    const std::string file = "tmpFile";
    const std::string num = "123456789";
    const std::string chars = "ABC";
    ss << chars << num << chars;
    Lexer::IStreamPtr istream{&ss};

    Lexer lexer;
    lexer.pushIStream(file, istream);

    const auto & tokenId = Token::TokenId::newId();

    auto & context = lexer.context();
    context.matchers().push_back(makeRegExpMatcher(
        "([0-9]*)",
        {tokenId},
        Context::defaultContext()
    ));
    context.matchers().push_back(makeStringMatcher(
        chars,
        {tokenId},
        Context::defaultContext()
    ));

    if (!lexer.hasToken()) return false;
    const auto & tokenChars = lexer.token();
    if (!lexer.hasToken()) return false;
    const auto & tokenNum = lexer.token();
    if (!lexer.hasToken()) return false;
    const auto & tokenChars2 = lexer.token();
    if (lexer.hasToken()) return false;

    if (tokenChars.id() != tokenId) return false;
    if (tokenChars.sourceInfo() != SourceInfo(file, 1, 1)) return false;
    if (tokenChars.value() != num) return false;

    if (tokenNum.id() != tokenId) return false;
    if (tokenNum.sourceInfo() != SourceInfo(file, 1, chars.size() + 1)) return false;
    if (tokenNum.value() != num) return false;

    if (tokenChars2.id() != tokenId) return false;
    if (tokenChars2.sourceInfo() != SourceInfo(file, 1, chars.size() + num.size() + 1)) return false;
    if (tokenChars2.value() != num) return false;

    if (lexer.currentSource() != SourceInfo(file, 1, chars.size() + num.size() + chars.size() + 1)) return false;

    std::cout << "test_moreMatches(): success" << std::endl;
    return true;
}

} // namespace

auto main() -> int
{
    if (!test_matchRegexp()) return EXIT_FAILURE;
    if (!test_noMatchRegexp()) return EXIT_FAILURE;
    if (!test_skipMatchRegexp()) return EXIT_FAILURE;
    if (!test_moreMatches()) return EXIT_FAILURE;
    return EXIT_SUCCESS;
}
