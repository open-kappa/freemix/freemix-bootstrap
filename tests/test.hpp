#include <functional>
#include <tuple>

using Code = uint64_t;

struct Info
{
    freemix::common::SourceInfo source;
    Code code;
};

using InfoList = std::vector<Info>;

template<typename ... T>
struct ParseResult
{
    bool matched = false;
    std::tuple<T...> ast;
    InfoList _warnings;
    InfoList _errors;
};

template<typename OutputAst>
class Terminal final
{
public:



};

template<typename OutputAst, typename ... InputAsts>
class NonTerminal final
{
public:
};
