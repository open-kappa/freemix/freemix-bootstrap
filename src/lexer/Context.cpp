#include "../../include/lexer/Context.hpp"

namespace freemix::lexer {

const auto KEEP_CONTEXT = Context::ContextId::newId();
const auto DEFAULT_CONTEXT = Context::ContextId::newId();

Context::Context() noexcept:
    _id(),
    _matchers()
{}

Context::~Context() noexcept
{}

Context::Context(const Context & other):
    _id(other._id),
    _matchers(other._matchers)
{}

Context::Context(Context && other) noexcept:
    _id(std::move(other._id)),
    _matchers(std::move(other._matchers))
{}

auto Context::operator =(const Context & other) -> Context &
{
    Context tmp(other);
    swap(tmp);
    return *this;
}

auto Context::operator =(Context && other) noexcept -> Context &
{
    Context tmp{std::move(other)};
    swap(tmp);
    return *this;
}

auto Context::swap(Context & other) noexcept -> void
{
    using std::swap;
    swap(_id, other._id);
    swap(_matchers, other._matchers);
}

auto Context::id() const -> ContextId
{
    return _id;
}

auto Context::matchers() const noexcept -> const Matchers &
{
    return _matchers;
}

auto Context::matchers() noexcept -> Matchers &
{
    return _matchers;
}


auto Context::noContext() noexcept -> ContextId
{
    return ContextId();
}

auto Context::keepContext() noexcept -> ContextId
{
    return KEEP_CONTEXT;
}

auto Context::defaultContext() noexcept -> ContextId
{
    return DEFAULT_CONTEXT;
}

auto swap(Context & o1, Context & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::lexer
