#include "../../include/lexer/Lexer.hpp"
#include "../../include/lexer/MatchResult.hpp"
#include <ranges>


namespace freemix::lexer {

Lexer::Lexer() noexcept:
    _contextMap(),
    _inputs(),
    _inTokens(),
    _stack(),
    _currentLine(),
    _currentContext(),
    _sourceInfo()
{}

Lexer::~Lexer() noexcept
{}

Lexer::Lexer(const Lexer & other):
    _contextMap(other._contextMap),
    _inputs(other._inputs),
    _inTokens(other._inTokens),
    _stack(other._stack),
    _currentLine(other._currentLine),
    _currentContext(other._currentContext),
    _sourceInfo(other._sourceInfo)
{}

Lexer::Lexer(Lexer && other) noexcept:
    _contextMap(std::map(other._contextMap)),
    _inputs(std::move(other._inputs)),
    _inTokens(std::move(other._inTokens)),
    _stack(std::move(other._stack)),
    _currentLine(std::move(other._currentLine)),
    _currentContext(std::move(other._currentContext)),
    _sourceInfo(std::move(other._sourceInfo))
{}

auto Lexer::operator =(const Lexer & other) -> Lexer &
{
    Lexer tmp(other);
    swap(tmp);
    return *this;
}

auto Lexer::operator =(Lexer && other) noexcept -> Lexer &
{
    Lexer tmp{std::move(other)};
    swap(tmp);
    return *this;
}

auto Lexer::swap(Lexer & other) noexcept -> void
{
    using std::swap;
    swap(_contextMap, other._contextMap);
    swap(_inTokens, other._inTokens);
    swap(_inputs, other._inputs);
    swap(_stack, other._stack);
    swap(_currentLine, other._currentLine);
    swap(_currentContext, other._currentContext);
    swap(_sourceInfo, other._sourceInfo);
}

auto Lexer::pushIStream(
    const std::string & file,
    const IStreamPtr & istream
) -> void
{
    _inputs.push_back({
        .file = file,
        .stream = istream
    });
}

auto Lexer::hasToken() -> bool
{
    if (_inTokens.empty()) _tokenize();
    return !_inTokens.empty();
}

auto Lexer::token() -> const Token &
{
    if (!hasToken()) throw std::exception();
    if (_stack.empty()) _stack.push_back({});
    _stack.back().tokens.push_back(_inTokens.front());
    _inTokens.pop_front();
    return _stack.back().tokens.back();
}

auto Lexer::untoken() -> void
{
    if (_stack.empty()) return;
    if (!_stack.back().tokens.empty()) return;
    auto & last = _stack.back();
    _inTokens.push_front(last.tokens.back());
    last.tokens.pop_back();
}

auto Lexer::pushStackTokens() -> void
{
    _stack.push_back({});
}

auto Lexer::popStackTokens() -> void
{
    if (_stack.empty()) return;
    auto & last = _stack.back();
    for (auto & token : std::ranges::reverse_view(last.tokens))
    {
        _inTokens.push_front(token);
    }
    _stack.pop_back();
}

auto Lexer::scopedContext() -> ScopedGuard
{
    const auto foo = [this]() -> void
    {
        this->popStackTokens();
    };
    pushStackTokens();
    return ScopedGuard{foo};
}

auto Lexer::currentSource() const noexcept -> const SourceInfo &
{
    return _sourceInfo;
}

auto Lexer::currentContext() const -> const Context &
{
    const auto it = _contextMap.find(_currentContext);
    if (it == _contextMap.cend()) throw std::exception();
    return it->second;
}

auto Lexer::context(const ContextId & id) const -> const Context &
{
    auto it = _contextMap.find(id);
    if (it == _contextMap.end())
    {
        Context context{id};
        _contextMap[id] = context;
        it = _contextMap.find(id);
    }
    return it->second;
}

auto Lexer::context(const ContextId & id) -> Context &
{
    auto it = _contextMap.find(id);
    if (it == _contextMap.end())
    {
        Context context{id};
        _contextMap[id] = context;
        it = _contextMap.find(id);
    }
    return it->second;
}

auto Lexer::context() const -> const Context &
{
    return context(Context::defaultContext());
}

auto Lexer::context() -> Context &
{
    return context(Context::defaultContext());
}

auto Lexer::_fetchLine() -> void
{
    std::string str;
    while (true)
    {
        if (_inputs.empty()) return;
        auto input = _inputs.back();
        _sourceInfo.file(input.file);
        str.clear();
        std::getline(*input.stream, str);
        if (!str.empty()) break;
        _inputs.pop_back();
        _sourceInfo.line(0);
    }

    _currentLine = str + "\n";
    _sourceInfo.line(_sourceInfo.line() + 1);
    _sourceInfo.column(1);
}

auto Lexer::_tokenize() -> void
{
    while (true)
    {
        if (_currentLine.empty()) _fetchLine();
        if (_currentLine.empty()) return;
        const auto & context = currentContext();
        const auto & matchers = context.matchers();
        for (const auto & match: matchers)
        {
            const auto & result = match(*this);
            if (!result.matched) continue;
            const auto readCols = _currentLine.size() - result.remaining.size();
            _sourceInfo.column(_sourceInfo.column() + readCols);
            _currentLine = result.remaining;
            for (const auto & token: result.tokens)
            {
                _inTokens.push_back(token);
            }
            if (result.nextContext != Context::keepContext())
            {
                _currentContext = result.nextContext;
            }
            if (!result.tokens.empty()) return;
        }
    }
}

auto swap(Lexer & o1, Lexer & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::lexer
