/* NOLINTBEGIN(modernize-pass-by-value) */
#include "../../include/lexer/Token.hpp"

namespace freemix::lexer {

Token::Token() noexcept:
    _sourceInfo(),
    _id(),
    _value()
{}

Token::~Token() noexcept
{}

Token::Token(const Token & other):
    _sourceInfo(other._sourceInfo),
    _id(other._id),
    _value(other._value)
{}

Token::Token(Token && other) noexcept:
    _sourceInfo(std::move(other._sourceInfo)),
    _id(std::move(other._id)),
    _value(std::move(other._value))
{}

auto Token::operator =(const Token & other) -> Token &
{
    Token tmp(other);
    swap(tmp);
    return *this;
}

auto Token::operator =(Token && other) noexcept -> Token &
{
    Token tmp{std::move(other)};
    swap(tmp);
    return *this;
}

auto Token::swap(Token & other) noexcept -> void
{
    using std::swap;
    swap(_sourceInfo, other._sourceInfo);
    swap(_id, other._id);
    swap(_value, other._value);
}

Token::Token(
    const SourceInfo & sourceInfo,
    const TokenId & id,
    const Value & value
):
    _sourceInfo(sourceInfo),
    _id(id),
    _value(value)
{}

auto Token::sourceInfo() const noexcept -> const SourceInfo &
{
    return _sourceInfo;
}

auto Token::sourceInfo(const SourceInfo & sourceInfo) noexcept -> Token &
{
    _sourceInfo = sourceInfo;
    return *this;
}

auto Token::id() const noexcept -> const TokenId &
{
    return _id;
}

auto Token::id(const TokenId & id) noexcept -> Token &
{
    _id = id;
    return *this;
}

auto Token::value() const noexcept -> const Value &
{
    return _value;
}

auto Token::value(const Value & value) noexcept -> Token &
{
    _value = value;
    return *this;
}

auto Token::operator ==(const Token & other) const noexcept -> bool
{
    return _id == other._id
        && _value == other._value;
}

auto Token::operator !=(const Token & other) const noexcept -> bool
{
    return !(*this == other);
}

auto Token::operator <(const Token & other) const noexcept -> bool
{
    if (_id < other._id) return true;
    else if (_id > other._id) return false;
    else if (_value < other._value) return true;
    else if (_value > other._value) return false;
    return false;
}

auto Token::operator <=(const Token & other) const noexcept -> bool
{
    return *this == other || *this < other;
}

auto Token::operator >(const Token & other) const noexcept -> bool
{
    return other < *this;
}

auto Token::operator >=(const Token & other) const noexcept -> bool
{
    return other <= *this;
}

auto Token::operator <=>(const Token & other) const noexcept -> std::strong_ordering
{
    if (*this == other) return std::strong_ordering::equivalent;
    else if (*this <= other) return std::strong_ordering::less;
    return std::strong_ordering::greater;
}

auto Token::noTokenId() noexcept -> TokenId
{
    return TokenId();
}

auto swap(Token & o1, Token & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::lexer

/* NOLINTEND(modernize-pass-by-value) */
