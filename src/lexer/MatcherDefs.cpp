#include "../../include/lexer/MatcherDefs.hpp"
#include "../../include/lexer/Lexer.hpp"
#include "../../include/lexer/MatchResult.hpp"
#include <regex>


namespace freemix::lexer {

auto makeRegExpMatcher(
    const std::string & regexp,
    const std::vector<Token::TokenId> & ids,
    const Context::ContextId & nextContext
) -> Matcher
{
    return [ids, regexp, nextContext](Lexer & lexer) -> MatchResult
    {
        const auto & line = lexer.currentLine();
        std::smatch match;
        const auto matched = std::regex_search(line, match, std::regex(regexp));
        const auto & fullMatch = match[0];
        if (!matched || (fullMatch.first - line.begin() != 0)) return {};
        const auto size = match.size();
        if (size != ids.size() + 1) return {};
        const auto & sourceInfo = lexer.currentSource();
        MatchResult ret = {
            .remaining = line.substr(size_t(fullMatch.second - line.begin())),
            .tokens = {},
            .nextContext = nextContext,
            .matched = true
        };
        for (auto i = std::size_t(1); i < size; ++i)
        {
            auto && mm = match[i];
            if (!mm.matched) return {};
            auto info = sourceInfo;
            const auto first = size_t(mm.first - line.begin());
            info.column += first;
            ret.tokens.emplace_back(Token()
                .sourceInfo(info)
                .id(ids[i - 1])
                .value(line.substr(first, size_t(mm.second - mm.first)))
            );
        }
        return ret;
    };
}

auto makeStringMatcher(
    const std::string & str,
    const Token::TokenId & id,
    const Context::ContextId & nextContext
) -> Matcher
{
    return [id, str, nextContext](Lexer & lexer) -> MatchResult
    {
        const auto & line = lexer.currentLine();
        if (!line.starts_with(str)) return {};
        return {
            .remaining = line.substr(str.size()),
            .tokens = {
                Token()
                    .id(id)
                    .value(str)
                    .sourceInfo(lexer.currentSource())
            },
            .nextContext = nextContext,
            .matched = true
        };
    };
}

} // namespace freemix::lexer
