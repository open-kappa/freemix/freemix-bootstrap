/* NOLINTBEGIN(modernize-pass-by-value) */
#include "../../include/common/ScopedGuard.hpp"

namespace freemix::common {

ScopedGuard::ScopedGuard() noexcept:
    _callback()
{}

ScopedGuard::~ScopedGuard()
{
    if (_callback) _callback();
}

ScopedGuard::ScopedGuard(ScopedGuard && other) noexcept:
    _callback(std::move(other._callback))
{
    other._callback = nullptr;
}

auto ScopedGuard::operator =(ScopedGuard && other) noexcept -> ScopedGuard &
{
    ScopedGuard tmp{std::move(other)};
    swap(tmp);
    return *this;
}

auto ScopedGuard::swap(ScopedGuard & other) noexcept -> void
{
    using std::swap;
    swap(_callback, other._callback);
}

ScopedGuard::ScopedGuard(const Callback & cb) noexcept:
    _callback(cb)
{}

auto ScopedGuard::release() noexcept -> void
{
    _callback = nullptr;
}

auto swap(ScopedGuard & o1, ScopedGuard & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::common

/* NOLINTEND(modernize-pass-by-value) */
