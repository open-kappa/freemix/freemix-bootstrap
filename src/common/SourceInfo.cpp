/* NOLINTBEGIN(modernize-pass-by-value) */
#include "../../include/common/SourceInfo.hpp"

namespace freemix::common {

SourceInfo::SourceInfo() noexcept:
    _file(),
    _line(0),
    _column(0)
{}

SourceInfo::~SourceInfo() noexcept
{}

SourceInfo::SourceInfo(const SourceInfo & other):
    _file(other._file),
    _line(other._line),
    _column(other._column)
{}

SourceInfo::SourceInfo(SourceInfo && other) noexcept:
    _file(std::move(other._file)),
    _line(std::move(other._line)),
    _column(std::move(other._column))
{}

auto SourceInfo::operator =(const SourceInfo & other) -> SourceInfo &
{
    SourceInfo tmp(other);
    swap(tmp);
    return *this;
}
auto SourceInfo::operator =(SourceInfo && other) noexcept -> SourceInfo &
{
    SourceInfo tmp{std::move(other)};
    swap(tmp);
    return *this;
}

auto SourceInfo::swap(SourceInfo & other) noexcept -> void
{
    using std::swap;
    swap(_file, other._file);
    swap(_line, other._line);
    swap(_column, other._column);
}

SourceInfo::SourceInfo(
    const std::string & file,
    const Size line,
    const Size column
):
    _file(file),
    _line(line),
    _column(column)
{}

auto SourceInfo::operator ==(const SourceInfo & other) const noexcept -> bool
{
    return _file == other._file
        && _line == other._line
        && _column == other._column;
}

auto SourceInfo::operator !=(const SourceInfo & other) const noexcept -> bool
{
    return !(*this == other);
}

auto SourceInfo::file() const noexcept -> const std::string &
{
    return _file;
}

auto SourceInfo::line() const noexcept -> Size
{
    return _line;
}

auto SourceInfo::column() const noexcept -> Size
{
    return _column;
}

auto SourceInfo::file(const std::string & file) -> SourceInfo &
{
    _file = file;
    return *this;
}

auto SourceInfo::line(const Size line) noexcept -> SourceInfo &
{
    _line = line;
    return *this;
}

auto SourceInfo::column(const Size column) noexcept -> SourceInfo &
{
    _column = column;
    return *this;
}


auto swap(SourceInfo & o1, SourceInfo & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::common

/* NOLINTEND(modernize-pass-by-value) */
