#include "../../include/parser/Ast.hpp"

namespace freemix::parser {

Ast::~Ast() noexcept
{}

Ast::Ast() noexcept
{}

Ast::Ast(const Ast & /*other*/)
{}

Ast::Ast(Ast && /*other*/) noexcept
{}

auto Ast::_swap(Ast & /*other*/) -> void
{}

} // namespace freemix::parser
