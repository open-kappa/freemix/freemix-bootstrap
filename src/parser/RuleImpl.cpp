#include "../../include/parser/RuleImpl.hpp"
#include "../../include/parser/Ast.hpp"
#include "../../include/parser/Parser.hpp"

namespace freemix::parser {

RuleImpl::RuleImpl() noexcept:
    _name(),
    _matchers(),
    _action()
{}

RuleImpl::~RuleImpl() noexcept
{}

RuleImpl::RuleImpl(const RuleImpl & other):
    _name(other._name),
    _matchers(other._matchers),
    _action(other._action)
{}

RuleImpl::RuleImpl(RuleImpl && other) noexcept:
    _name(std::move(other._name)),
    _matchers(std::move(other._matchers)),
    _action(std::move(other._action))
{}

auto RuleImpl::operator =(const RuleImpl & other) -> RuleImpl &
{
    RuleImpl tmp(other);
    swap(tmp);
    return *this;
}

auto RuleImpl::operator =(RuleImpl && other) noexcept -> RuleImpl &
{
    RuleImpl tmp(std::move(other));
    swap(tmp);
    return *this;
}

auto RuleImpl::swap(RuleImpl & other) noexcept -> void
{
    using std::swap;
    swap(_name, other._name);
    swap(_matchers, other._matchers);
    swap(_action, other._action);
}

auto RuleImpl::operator()(Parser & parser) const -> ParseResult<Ast>
{
    ParseResult<Ast> partial;
    auto guard = parser.lexer().scopedContext();
    for (const auto & matcher: _matchers)
    {
        const auto tmp = (*matcher)(parser);
        if (!tmp.matched)
        {
            partial.ast.clear();
            return partial;
        }
        for (const auto & node: tmp.ast)
        {
            partial.ast.push_back(node);
        }
    }
    partial.matched = true;
    if (!_action)
    {
        guard.release();
        return partial;
    }
    auto && ret = _action(partial.ast);
    guard.release();
    return ret;
}

auto RuleImpl::name() const -> const std::string &
{
    return _name;
}

auto RuleImpl::name(const std::string & name) -> RuleImpl &
{
    _name = name;
    return *this;
}

auto RuleImpl::matchers() const -> const Matchers &
{
    return _matchers;
}

auto RuleImpl::matchers() -> Matchers
{
    return _matchers;
}

auto RuleImpl::action() const noexcept -> const Action &
{
    return _action;
}

auto RuleImpl::action(const Action & action) -> RuleImpl &
{
    _action = action;
    return *this;
}

auto swap(RuleImpl & o1, RuleImpl & o2) noexcept -> void
{
    o1.swap(o2);
}

} // namespace freemix::parser
